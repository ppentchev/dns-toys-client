#!/bin/sh

set -e

cargo='cargo'
nursery=1

while getopts 'c:n' o; do
	case "$o" in
		c)
			cargo="$OPTARG"
			;;

		n)
			unset nursery
			;;

		*)
			echo 'whee [-c cargo] [-n]' 1>&2
			exit 1
			;;
	esac
done
shift "$((OPTIND - 1))"

set -x
"$cargo" clippy -- \
	-W clippy::restriction -A clippy::implicit_return -A clippy::self_named_module_files \
	-W clippy::pedantic \
	${nursery+-W clippy::nursery} \
	-W clippy::cargo \
	# That's all, folks!
