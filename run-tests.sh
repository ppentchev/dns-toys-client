#!/bin/sh

set -e

: "${PROG:=target/debug/dns-toys-client-sync}"

"$PROG" --help --version

"$PROG" features

feature-check -l -O 'features' -o json -- "$PROG"

"$PROG" -d -a 138.197.68.199 -j time Sofia Basseterre

"$PROG" -d time Sofia Basseterre

if "$PROG" -v -a 138.197.68.199 time Sofiaaaaaa || [ "$?" != 1 ]; then
	echo 'This one was not supposed to be found!' 1>&2
	exit 1
fi

if "$PROG" -v -j time Sofiaaaaaa || [ "$?" != 1 ]; then
	echo 'This one was not supposed to be found!' 1>&2
	exit 1
fi

# This one is still a work in progress; it seems that trust-dns-client will
# absolutely refuse to let us use a slash within a DNS name label.
#
if "$PROG" -j -a 138.197.68.199 time Sofia/US || [ "$?" != 2 ]; then
	echo 'This one was supposed to fail!' 1>&2
	exit 1
fi

if "$PROG" time Sofia/US || [ "$?" != 2 ]; then
	echo 'This one was supposed to fail!' 1>&2
	exit 1
fi

"$PROG" -j -d weather Sofia Basseterre

shellcheck run-tests.sh run-clippy.sh
