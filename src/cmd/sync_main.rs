#![deny(missing_docs)]
//! dns-toys-client - query the dns.toys API
//!
//! Invoke the library crate's functions, display the output.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::net::SocketAddr;

use expect_exit::{Expected, ExpectedWithError};
use itertools::Itertools;
use tracing::trace;
use trust_dns_resolver::config::{ResolverConfig, ResolverOpts};
use trust_dns_resolver::Resolver;

use dns_toys_client::sync::ToysClient;
use dns_toys_client::{City, Response, Time, ToysError, Weather};

mod cli;
mod common;
mod defs;

use defs::{Mode, Output, Query, QueryKind, ToysResult};

/// Display the current time in the specified city.
#[tracing::instrument(skip(client))]
fn show_time(client: ToysClient, cities: &[City]) -> Output {
    trace!("About to fetch the time for {}", cities.join(", "));
    common::process_response(
        "city",
        cities
            .iter()
            .map(|city| client.get_time(city).map(Response::into_data))
            .flatten_ok()
            .collect::<Result<Vec<Time>, ToysError>>(),
        Output::Time,
    )
}

/// Display the weather forecast for the specified city.
#[tracing::instrument(skip(client))]
fn show_weather(client: ToysClient, cities: &[City]) -> Output {
    trace!(
        "About to fetch the weather forecast for {}",
        cities.join(", ")
    );
    common::process_response(
        "city",
        cities
            .iter()
            .map(|city| client.get_weather(city).map(Response::into_data))
            .flatten_ok()
            .collect::<Result<Vec<Weather>, ToysError>>(),
        Output::Weather,
    )
}

/// Determine the address of the dns.toys server.
#[tracing::instrument]
fn find_server(query: &Query) -> Result<SocketAddr, String> {
    match query.address {
        Some(ref address) => {
            trace!("Using the user-supplied address {} for dns.toys", address);
            Ok(format!("{}:53", address).parse().map_err(|err| {
                format!("Could not parse '{}' as an IP address: {}", address, err)
            })?)
        }
        None => {
            let resolver = Resolver::new(ResolverConfig::default(), ResolverOpts::default())
                .or_exit_e_("Could not set up a DNS resolver object");
            let resp = resolver
                .lookup_ip("dns.toys")
                .or_exit_e_("Could not look up 'dns.toys'");
            let addr = resp
                .iter()
                .next()
                .or_exit_("Could not find even a single address record for 'dns.toys'");
            trace!("Got an address for dns.toys: {:?}", addr);
            Ok(SocketAddr::new(addr, 53))
        }
    }
}

fn main() -> ToysResult {
    let mode = cli::parse_args();
    match mode {
        Mode::Handled(res) => res,
        Mode::Query(query) => {
            common::setup_logging(&query);

            common::output(&query, &{
                match find_server(&query) {
                    Ok(server) => match ToysClient::new(server) {
                        Ok(client) => match query.kind {
                            QueryKind::Time(ref cities) => show_time(client, cities),
                            QueryKind::Weather(ref cities) => show_weather(client, cities),
                        },
                        Err(err) => Output::NoApiClient(err.to_string()),
                    },
                    Err(err) => Output::NoApiServer(err),
                }
            })
        }
    }
}
