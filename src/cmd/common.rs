//! Common routines for the dns-toys-client command-line tools.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::io;

use expect_exit::ExpectedWithError;
use serde::Serialize;
use tracing::level_filters::LevelFilter;

use dns_toys_client::ToysError;

use crate::defs::{Output, OutputFormat, Query, ToysResult};

/// The version of the JSON output format.
#[derive(Debug, Serialize)]
struct JsonTopLevelFormatVersion {
    /// The major version number.
    major: u32,
    /// The minor version number.
    minor: u32,
}

/// The JSON output format descriptor.
#[derive(Debug, Serialize)]
struct JsonTopLevelFormat {
    /// The version of the JSON output format.
    version: JsonTopLevelFormatVersion,
}

/// The top-level JSON object to display.
#[derive(Debug, Serialize)]
struct JsonTopLevel<'data, Q, O> {
    /// The output format descriptor.
    format: JsonTopLevelFormat,
    /// The query sent.
    query: &'data Q,
    /// The operation result.
    result: ToysResult,
    /// The data to output.
    output: &'data O,
}

impl<'data, Q, O> JsonTopLevel<'data, Q, O> {
    /// Create a top-level object with the default format version.
    const fn new(query: &'data Q, result: ToysResult, output: &'data O) -> Self {
        Self {
            format: JsonTopLevelFormat {
                version: JsonTopLevelFormatVersion { major: 0, minor: 1 },
            },
            query,
            result,
            output,
        }
    }
}

/// Set up program and library logging as requested.
pub fn setup_logging(query: &Query) {
    if query.log_level == LevelFilter::OFF {
        return;
    }

    let subscriber = tracing_subscriber::fmt()
        .with_max_level(query.log_level.into_level())
        .with_writer(io::stderr)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .or_exit_e_("Could not set up the tracing global subscriber");
}

/// Format the output as requested.
#[allow(clippy::print_stderr)]
#[allow(clippy::print_stdout)]
pub fn output(query: &Query, data: &Output) -> ToysResult {
    let res = data.to_toys_result();
    match query.output_format {
        OutputFormat::Json => {
            let top = JsonTopLevel::new(query, res, data);
            let joutput = serde_json::to_string_pretty(&top)
                .or_exit_e_("Could not format the output as a JSON object");
            println!("{}", joutput);
        }
        OutputFormat::Tabs => match *data {
            Output::ApiError(ref err) => {
                eprintln!("The server reported an error: {}", err);
            }
            Output::DnsError(ref err) => {
                eprintln!("Could not send the query to dns.toys: {}", err);
            }
            Output::NotFound(ref what, ref name) => {
                eprintln!(
                    "The server does not recognize '{}' as a valid {}",
                    name, what
                );
            }
            Output::Time(ref time) => {
                for item in time {
                    println!(
                        "{}\t{}\t{}\t{}",
                        item.city(),
                        item.tz_name(),
                        item.country_code(),
                        item.time()
                    );
                }
            }
            Output::NoApiClient(ref err) => {
                eprintln!("Could not set up our dns.toys client class: {}", err);
            }
            Output::NoApiServer(ref err) => {
                eprintln!("Could not find the address for dns.toys: {}", err);
            }
            Output::NotImplemented(ref what) => {
                eprintln!("Not implemented yet: {}", what);
            }
            Output::Weather(ref weather) => {
                for item in weather {
                    println!(
                        "{}\t{}\t{}\t{}\t{}\t{}",
                        item.city(),
                        item.temp_celsius(),
                        item.temp_fahrenheit(),
                        item.humidity(),
                        item.weather(),
                        item.time(),
                    );
                }
            }
        },
    }
    res
}

/// Map `dns_toys_client` errors to Output values.
pub fn process_response<T, F: FnOnce(T) -> Output>(
    what: &str,
    res: Result<T, ToysError>,
    handle: F,
) -> Output {
    match res {
        Ok(data) => handle(data),
        Err(ToysError::ApiNotFound(item)) => Output::NotFound(what.to_owned(), item),
        Err(ToysError::ApiError(err)) => Output::ApiError(err),
        Err(
            err @ (ToysError::AsyncClient(_)
            | ToysError::Decode(_, _)
            | ToysError::NameFormat(_, _)
            | ToysError::Parse(_, _)
            | ToysError::Send(_, _)
            | ToysError::UdpConn(_)),
        ) => Output::DnsError(err.to_string()),
        Err(ToysError::NotImplemented(err)) => Output::NotImplemented(err),
    }
}
