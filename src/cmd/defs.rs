//! Common definitions for the dns.toys command-line clients.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
// serde::Serialize seems to have some trouble with this one...
// https://github.com/serde-rs/serde/pull/2221
#![allow(clippy::use_self)]

use std::process::{ExitCode, Termination};

use serde::Serialize;
use tracing::level_filters::LevelFilter;

use dns_toys_client::{Time, Weather};

/// The exit code of the command-line tool.
#[derive(Debug, Clone, Copy, Serialize)]
#[repr(u8)]
pub enum ToysResult {
    /// It's all good, man.
    Success = 0,
    /// The server does not know what we are asking it about.
    NotFound = 1,
    /// Could not send the query for some reason.
    Failed = 2,
    /// Well, nobody's perfect!
    NotImplemented = 42,
}

impl Termination for ToysResult {
    #[allow(clippy::as_conversions)]
    fn report(self) -> ExitCode {
        ExitCode::from(self as u8)
    }
}

/// The output produced by a query.
#[derive(Debug, Serialize)]
pub enum Output {
    /// The server reported an error.
    ApiError(String),
    /// An error that occurred while sending the DNS query.
    DnsError(String),
    /// Could not set up our API client for the specified server.
    NoApiClient(String),
    /// Could not find the server address for dns.toys.
    NoApiServer(String),
    /// The server does not know what we are asking it about.
    NotFound(String, String),
    /// Not done yet, and that's that!
    NotImplemented(String),
    /// The time at the specified location.
    Time(Vec<Time>),
    /// The time at the specified location.
    Weather(Vec<Weather>),
}

/// The output format chosen by the user.
#[derive(Debug)]
pub enum OutputFormat {
    /// A single JSON object.
    Json,
    /// Tab-separated values on separate lines.
    Tabs,
}

impl Output {
    /// Obtain the exit status corresponding to the program output.
    pub const fn to_toys_result(&self) -> ToysResult {
        match *self {
            Self::Time(_) | Self::Weather(_) => ToysResult::Success,
            Self::NotFound(_, _) => ToysResult::NotFound,
            Self::ApiError(_) | Self::DnsError(_) | Self::NoApiClient(_) | Self::NoApiServer(_) => {
                ToysResult::Failed
            }
            Self::NotImplemented(_) => ToysResult::NotImplemented,
        }
    }
}

/// What should the command-line tool do?
#[derive(Debug)]
#[allow(clippy::exhaustive_enums)]
pub enum Mode {
    /// Nothing more, the requested information was provided (e.g. features).
    Handled(ToysResult),
    /// Send a query to the dns.toys server.
    Query(Query),
}

/// The query to send to the dns.toys server.
#[derive(Debug, Serialize)]
pub struct Query {
    /// The address to use for the dns.toys server, if specified.
    pub address: Option<String>,
    /// What query to send?
    pub kind: QueryKind,
    /// The log verbosity level.
    #[serde(skip)]
    pub log_level: LevelFilter,
    /// The output format requested.
    #[serde(skip)]
    pub output_format: OutputFormat,
}

/// What query to send to the dns.toys server?
#[derive(Debug, Serialize)]
pub enum QueryKind {
    /// Display the current time in the specified city.
    Time(Vec<String>),
    /// Display the weather forecast for the specified city.
    Weather(Vec<String>),
}
