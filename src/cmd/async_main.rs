//! dns-toys-client-async - a Tokio-based async client for dns.toys.
//!
//! Use the library functions, send the queries asynchronously.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::net::SocketAddr;
use std::vec::IntoIter as VecIntoIter;

use expect_exit::{Expected, ExpectedWithError};
use tokio::sync::mpsc::{self, Sender as MpscSender};
use tokio::sync::oneshot;
use tracing::{error, trace};
use trust_dns_resolver::config::{ResolverConfig, ResolverOpts};
use trust_dns_resolver::TokioAsyncResolver;

mod cli;
mod common;
mod defs;

use defs::{Mode, Output, Query, QueryKind, ToysResult};
use dns_toys_client::async_tokio::{Request as TCRequest, ToysClient};
use dns_toys_client::{Response, Time, ToysError, Weather};

/// Determine the address of the dns.toys server.
#[tracing::instrument]
async fn find_server(query: &Query) -> Result<SocketAddr, String> {
    match query.address {
        Some(ref address) => {
            trace!("Using the user-supplied address {} for dns.toys", address);
            Ok(format!("{}:53", address).parse().map_err(|err| {
                format!("Could not parse '{}' as an IP address: {}", address, err)
            })?)
        }
        None => {
            let resolver =
                TokioAsyncResolver::tokio(ResolverConfig::default(), ResolverOpts::default())
                    .or_exit_e_("Could not set up a DNS resolver object");
            let resp = resolver
                .lookup_ip("dns.toys")
                .await
                .or_exit_e_("Could not look up 'dns.toys'");
            let addr = resp
                .iter()
                .next()
                .or_exit_("Could not find even a single address record for 'dns.toys'");
            trace!("Got an address for dns.toys: {:?}", addr);
            Ok(SocketAddr::new(addr, 53))
        }
    }
}

/// A set of types and handler functions for sending a specific API query.
trait RequestSender {
    /// The API query data type.
    type Data: Send;

    /// An intermediate storage type during processing.
    type Store: Send;

    /// The human-readable name of the API query.
    fn what() -> &'static str;

    /// Convert a [`Response`] into an [`Output`] instance.
    fn handle(data: Self::Data) -> Output;

    /// Check whether the [`Output`] instance is a successful response to
    /// this specific query and, if so, convert it into an intermediate
    /// data type.
    ///
    /// # Errors
    /// If the [`Output`] instance is not a successful response, return it
    /// to the caller for further processing.
    fn check(msg: Output) -> Result<Self::Store, Output>;

    /// Convert intermediate values to the final [`Output`] instance.
    fn finalize(collected: Vec<Self::Store>) -> Output;
}

/// A request to send to the [`dns_toys_client::async_tokio`] routines.
type RequestDef<T> = (
    String,
    TCRequest,
    oneshot::Receiver<Result<Response<T>, ToysError>>,
);

/// Send a series of requests for one and the same API query, await and
/// process the responses.
async fn requests_loop<S>(
    tx_req: MpscSender<TCRequest>,
    requests: Vec<RequestDef<S::Data>>,
) -> Output
where
    S: RequestSender,
    <S as RequestSender>::Data: Send + 'static,
{
    let mut rx = {
        let (tx, rx) = mpsc::channel(16);

        let mut tasks = vec![];
        let mut failed = false;
        for (name, req, rx_time) in requests {
            let name_err = name.clone();
            let tx_task = tx.clone();
            match tx_req.send(req).await {
                Ok(()) => tasks.push(tokio::spawn(async move {
                    let resp = match rx_time.await {
                        Ok(Ok(resp)) => {
                            common::process_response(S::what(), Ok(resp.into_data()), S::handle)
                        }
                        Ok(Err(err)) => common::process_response(S::what(), Err(err), S::handle),
                        Err(err) => Output::DnsError(format!(
                            "Querying '{}' for '{}': {}",
                            S::what(),
                            name_err,
                            err
                        )),
                    };
                    if let Err(err) = tx_task.send(resp).await {
                        error!(
                            "Could not send the {} client result for {}: {}",
                            S::what(),
                            name_err,
                            err
                        );
                    }
                })),
                Err(err) => {
                    error!(
                        "Could not send a '{}' query for '{}': {}",
                        S::what(),
                        name_err,
                        err
                    );
                    failed = true;
                    break;
                }
            }
        }

        if failed {
            for task in tasks {
                task.abort();
            }
        }

        rx
    };

    let mut collected = vec![];
    while let Some(msg) = rx.recv().await {
        trace!("Got a message: {:?}", msg);
        match S::check(msg) {
            Ok(data) => collected.push(data),
            Err(res) => return res,
        }
    }
    S::finalize(collected)
}

/// The handler functions for sending a "time" query.
struct TimeRequestSender {}

impl TimeRequestSender {
    /// The human-readable short name of the query.
    const WHAT: &'static str = "time";
}

impl RequestSender for TimeRequestSender {
    type Data = Vec<Time>;
    type Store = VecIntoIter<Time>;

    fn what() -> &'static str {
        Self::WHAT
    }

    fn handle(lines: Self::Data) -> Output {
        Output::Time(lines)
    }

    fn check(msg: Output) -> Result<Self::Store, Output> {
        if let Output::Time(lines) = msg {
            Ok(lines.into_iter())
        } else {
            Err(msg)
        }
    }

    fn finalize(collected: Vec<Self::Store>) -> Output {
        Output::Time(collected.into_iter().flatten().collect())
    }
}

/// Send a current time query, parse the response.
#[tracing::instrument(skip(tx_req))]
async fn show_time(tx_req: MpscSender<TCRequest>, cities: &[String]) -> Output {
    trace!("About to fetch the time for {}", cities.join(", "));
    let requests: Vec<_> = cities
        .iter()
        .map(|city| {
            let (tx_time, rx_time) = oneshot::channel();
            (
                city.clone(),
                TCRequest::Time(city.clone(), tx_time),
                rx_time,
            )
        })
        .collect();
    trace!("Prepared some requests: {:?}", requests);
    requests_loop::<TimeRequestSender>(tx_req, requests).await
}

/// The handler functions for sending a "weather" query.
struct WeatherRequestSender {}

impl WeatherRequestSender {
    /// The human-readable short name of the query.
    const WHAT: &'static str = "weather";
}

impl RequestSender for WeatherRequestSender {
    type Data = Vec<Weather>;
    type Store = VecIntoIter<Weather>;

    fn what() -> &'static str {
        Self::WHAT
    }

    fn handle(lines: Self::Data) -> Output {
        Output::Weather(lines)
    }

    fn check(msg: Output) -> Result<Self::Store, Output> {
        if let Output::Weather(lines) = msg {
            Ok(lines.into_iter())
        } else {
            Err(msg)
        }
    }

    fn finalize(collected: Vec<Self::Store>) -> Output {
        Output::Weather(collected.into_iter().flatten().collect())
    }
}

/// Send a weather forecast query, parse the response.
#[tracing::instrument(skip(tx_req))]
async fn show_weather(tx_req: MpscSender<TCRequest>, cities: &[String]) -> Output {
    trace!(
        "About to fetch the weather forecast for {}",
        cities.join(", ")
    );
    let requests: Vec<_> = cities
        .iter()
        .map(|city| {
            let (tx_weather, rx_weather) = oneshot::channel();
            (
                city.clone(),
                TCRequest::Weather(city.clone(), tx_weather),
                rx_weather,
            )
        })
        .collect();
    trace!("Prepared some requests: {:?}", requests);
    requests_loop::<WeatherRequestSender>(tx_req, requests).await
}

#[tokio::main]
async fn main() -> ToysResult {
    let mode = cli::parse_args();
    match mode {
        Mode::Handled(res) => res,
        Mode::Query(query) => {
            common::setup_logging(&query);

            common::output(&query, &{
                match find_server(&query).await {
                    Ok(server) => match ToysClient::new(server).await {
                        Ok(mut client) => {
                            let (tx_req, rx_req) = mpsc::channel(16);
                            let cli_task =
                                tokio::spawn(async move { client.process(rx_req).await });
                            trace!("Spawned the async client loop task");
                            let res = match query.kind {
                                QueryKind::Time(ref cities) => {
                                    show_time(tx_req.clone(), cities).await
                                }
                                QueryKind::Weather(ref cities) => {
                                    show_weather(tx_req.clone(), cities).await
                                }
                            };

                            trace!("Telling the async client that we are done");
                            match tx_req.send(TCRequest::Done).await {
                                Ok(()) => {
                                    trace!("Waiting for the async client loop to end");
                                    #[allow(clippy::integer_arithmetic)]
                                    if let (Err(err),) = tokio::join!(cli_task) {
                                        if !err.is_cancelled() {
                                            error!("oof, what do we do with {:?}", err);
                                        }
                                    }
                                }
                                Err(err) => {
                                    error!("Could not tell the async client loop to end: {}", err);
                                }
                            };

                            res
                        }
                        Err(err) => Output::NoApiClient(err.to_string()),
                    },
                    Err(err) => Output::NoApiServer(err),
                }
            })
        }
    }
}
