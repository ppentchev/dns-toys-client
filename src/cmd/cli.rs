//! The command-line parsing routines for the dns-toys-client tool.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
// serde::Serialize seems to have some trouble with this one...
// https://github.com/serde-rs/serde/pull/2221
#![allow(clippy::use_self)]

use clap::{Parser, Subcommand};
use itertools::Itertools;
use tracing::level_filters::LevelFilter;

use dns_toys_client::City;

use crate::defs::{Mode, OutputFormat, Query, QueryKind, ToysResult};

/// The top-level parser for command-line options.
#[derive(Debug, Parser)]
#[clap(author, version, about)]
struct ToysCli {
    /// The address to use for the dns.toys server.
    #[clap(long, short)]
    address: Option<String>,
    /// Debug mode: display all the diagnostic messages.
    #[clap(long, short)]
    debug: bool,
    /// Display the results or errors as a JSON object.
    #[clap(long, short)]
    json: bool,
    /// Verbose mode: display informational messages.
    #[clap(long, short)]
    verbose: bool,
    /// The action for the command-line tool to take.
    #[clap(subcommand)]
    command: ToysCliCommand,
}

/// The action for the command-line tool to take.
#[derive(Debug, Subcommand)]
enum ToysCliCommand {
    /// Display the list of features supported by the program.
    Features,
    /// Fetch the time for the specified city.
    Time {
        /// The city to fetch the time for.
        cities: Vec<City>,
    },
    /// Fetch the weather forecast for the specified city.
    Weather {
        /// The city to fetch the forecast for.
        cities: Vec<City>,
    },
}

/// Display the features supported by the program.
#[allow(clippy::print_stdout)]
fn show_features() -> ToysResult {
    println!(
        "Features: {}",
        dns_toys_client::FEATURES
            .iter()
            .map(|&(name, ver)| format!("{}={}", name, ver))
            .join(" ")
    );
    ToysResult::Success
}

/// Parse the command-line arguments, handle "features".
pub fn parse_args() -> Mode {
    let args = ToysCli::parse();

    // Common parameters
    let address = args.address;
    let log_level = if args.debug {
        LevelFilter::TRACE
    } else if args.verbose {
        LevelFilter::INFO
    } else {
        LevelFilter::OFF
    };
    let output_format = if args.json {
        OutputFormat::Json
    } else {
        OutputFormat::Tabs
    };

    match args.command {
        ToysCliCommand::Features => Mode::Handled(show_features()),
        ToysCliCommand::Time { cities } => Mode::Query(Query {
            address,
            kind: QueryKind::Time(cities),
            log_level,
            output_format,
        }),
        ToysCliCommand::Weather { cities } => Mode::Query(Query {
            address,
            kind: QueryKind::Weather(cities),
            log_level,
            output_format,
        }),
    }
}
