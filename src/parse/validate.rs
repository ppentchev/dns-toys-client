//! Validation functions for the various dns.toys API fields.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use expect_exit::{Expected, ExpectedWithError};
use lazy_static::lazy_static;
use regex::{Captures, Regex};

/// Match a "City name (CC)" field.
pub fn re_city_country() -> &'static Regex {
    lazy_static! {
        static ref RE_CITY_COUNTRY: Regex = Regex::new(
            r"(?x)
    ^
    (?P<city> [^)]+? )
    \s+ [(]
    (?P<country_code> [A-Z]{2} )
    [)]
    $
"
        )
        .or_exit_e_("Internal error: could not compile the city/country regex");
    }

    &RE_CITY_COUNTRY
}

/// Match a "City name (Time/Zone, CC)" field.
pub fn re_city_tz_country() -> &'static Regex {
    lazy_static! {
        static ref RE_CITY_TZ_COUNTRY: Regex = Regex::new(
            r"(?x)
    ^
    (?P<city> [^)]+? )
    \s+ [(]
    (?P<tz> [^,)]+ )
    , \s+
    (?P<country_code> [A-Z]{2} )
    [)]
    $
"
        )
        .or_exit_e_("Internal error: could not compile the city/tz/country regex");
    }

    &RE_CITY_TZ_COUNTRY
}

/// Match a non-empty field with no further restrictions.
pub fn re_non_empty() -> &'static Regex {
    lazy_static! {
        static ref RE_NON_EMPTY: Regex =
            Regex::new("^.+$").or_exit_e_("Internal error: could not compile the non-empty regex");
    }

    &RE_NON_EMPTY
}

/// Extract a captured value by name as a [`String`] object.
pub trait ReCheckName<'name> {
    /// Fetch the captured value by name as a string.
    /// Exits the program if the value is not there.
    fn check_name(&self, name: &'name str) -> String;
}

impl<'caps, 'name: 'caps, 'res: 'name> ReCheckName<'res> for Captures<'caps> {
    fn check_name(&self, name: &'name str) -> String {
        self.name(name)
            .or_exit(|| format!("Internal error: no '{}' in {:?}", name, self))
            .as_str()
            .to_owned()
    }
}

/// Extract a captured value by index as a [`String`] object.
pub trait ReCheckIndex {
    /// Fetch the captured value by index as a string.
    /// Exits the program if the value is not there.
    fn check_index(&self, index: usize) -> String;
}

impl<'caps> ReCheckIndex for Captures<'caps> {
    fn check_index(&self, index: usize) -> String {
        self.get(index)
            .or_exit(|| format!("Internal error: no {} in {:?}", index, self))
            .as_str()
            .to_owned()
    }
}
