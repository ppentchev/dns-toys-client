//! Parse the response to a "current time in a city" query.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use tracing::{info, trace};

use crate::parse;
use crate::parse::validate::{self, ReCheckIndex, ReCheckName};
use crate::{City, Response, Time, ToysError, TxtParts, TxtRecords};

/// Parse the TXT records (the "answer" and "additional" sections).
///
/// # Errors
/// - [`crate::ToysError::ApiNotFound`] if the server returns no answers
/// - [`crate::ToysError::ApiError`] if the server returns unrecognized
///   text in the "additional" section
/// - [`crate::ToysError::Parse`] if the records in the "answer" section
///   are not in the expected format
#[allow(clippy::missing_inline_in_public_items)]
pub fn parse_txt(
    city: &City,
    name_str: &str,
    raw: TxtRecords,
    raw_add: &[TxtParts],
) -> Result<Response<Vec<Time>>, ToysError> {
    trace!(
        "Got a text response: answers {:?} additional {:?}",
        raw,
        raw_add
    );
    info!(
        "Got {} answers and {} additional lines for the time at {}",
        raw.len(),
        raw_add.len(),
        city
    );

    parse::parse_txt(
        &*city,
        name_str,
        raw,
        raw_add,
        "error: unknown city.",
        &[validate::re_city_tz_country(), validate::re_non_empty()],
        |caps| match *caps {
            [ref caps_location, ref caps_time] => Ok(Time {
                city: caps_location.check_name("city"),
                tz_name: caps_location.check_name("tz"),
                country_code: caps_location.check_name("country_code"),
                time: caps_time.check_index(0),
            }),
            _ => expect_exit::exit(&format!(
                "Internal error: how did the time result capture {:?}",
                caps
            )),
        },
    )
}
