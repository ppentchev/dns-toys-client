//! Parse the response to a "weather forecast for a city" query.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use expect_exit::ExpectedWithError;
use lazy_static::lazy_static;
use regex::Regex;
use tracing::{info, trace};

use crate::parse;
use crate::parse::validate::{self, ReCheckIndex, ReCheckName};
use crate::{City, Response, ToysError, TxtParts, TxtRecords, Weather};

/// Match a temperature specification in Celsius and Fahrenheit.
#[must_use]
#[inline]
pub fn re_temperature() -> &'static Regex {
    lazy_static! {
        static ref RE_TEMPERATURE: Regex = Regex::new(
            r"(?x)
    ^
    (?P<celsius>
        (?: 0 | [1-9][0-9]* )
        (?: \. [0-9]+ )?
    ) C
    \s+ [(]
    (?P<fahrenheit>
        (?: 0 | [1-9][0-9]* )
        (?: \. [0-9]+ )?
    ) F
    [)]
    $
"
        )
        .or_exit_e_("Internal error: could not compile the temperature regex");
    }

    &RE_TEMPERATURE
}

/// Match a humidity percentage display.
#[must_use]
#[inline]
pub fn re_humidity() -> &'static Regex {
    lazy_static! {
        static ref RE_HUMIDITY: Regex = Regex::new(
            r"(?x)
    ^
    (?P<humidity>
        (?: 0 | [1-9][0-9]* )
        (?: \. [0-9]+ )?
    ) [%]
    \s+ hu \.
    $
"
        )
        .or_exit_e_("Internal error: could not compile the humidity regex");
    }

    &RE_HUMIDITY
}

/// Parse the TXT records (the "answer" and "additional" sections).
///
/// # Errors
/// - [`crate::ToysError::ApiNotFound`] if the server returns no answers
/// - [`crate::ToysError::ApiError`] if the server returns unrecognized
///   text in the "additional" section
/// - [`crate::ToysError::Parse`] if the records in the "answer" section
///   are not in the expected format
#[allow(clippy::missing_inline_in_public_items)]
pub fn parse_txt(
    city: &City,
    name_str: &str,
    raw: TxtRecords,
    raw_add: &[TxtParts],
) -> Result<Response<Vec<Weather>>, ToysError> {
    trace!(
        "Got a text response: answers {:?} additional {:?}",
        raw,
        raw_add
    );
    info!(
        "Got {} answers and {} additional lines for the weather at {}",
        raw.len(),
        raw_add.len(),
        city
    );

    parse::parse_txt(
        &*city,
        name_str,
        raw,
        raw_add,
        "error: unknown city.",
        &[
            validate::re_city_country(),
            re_temperature(),
            re_humidity(),
            validate::re_non_empty(),
            validate::re_non_empty(),
        ],
        |caps| match *caps {
            [ref caps_city, ref caps_temp, ref caps_humidity, ref caps_weather, ref caps_time] => {
                Ok(Weather {
                    city: caps_city.check_name("city"),
                    country_code: caps_city.check_name("country_code"),
                    temp_celsius: caps_temp
                        .check_name("celsius")
                        .parse()
                        .or_exit_e_("Internal error: could not parse a floating point number"),
                    temp_fahrenheit: caps_temp
                        .check_name("fahrenheit")
                        .parse()
                        .or_exit_e_("Internal error: could not parse a floating point number"),
                    humidity: caps_humidity
                        .check_name("humidity")
                        .parse()
                        .or_exit_e_("Internal error: could not parse a floating point number"),
                    weather: caps_weather.check_index(0),
                    time: caps_time.check_index(0),
                })
            }
            _ => expect_exit::exit(&format!(
                "Internal error: how did the weather result capture {:?}",
                caps
            )),
        },
    )
}
