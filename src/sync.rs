//! A synchronous interface for fetching dns.toys data.

use std::net::SocketAddr;
use std::str::FromStr;

use tracing::trace;
use trust_dns_client::client::{Client, SyncClient};
use trust_dns_client::rr::{DNSClass, Name, RecordType};
use trust_dns_client::udp::UdpClientConnection;

use crate::parse::time as parse_time;
use crate::parse::weather as parse_weather;
use crate::{City, Response, Time, ToysError, TxtRecords, Weather};

/// A client for synchronously sending multiple queries to the dns.toys API.
pub struct ToysClient {
    /// The trust-dns-client object for sending the queries.
    client: SyncClient<UdpClientConnection>,
    /// The server's address.
    server: SocketAddr,
}

impl ToysClient {
    /// Prepare to send queries to the dns.toys API server at the specified address.
    ///
    /// # Errors
    /// Could not prepare a UDP connection to the specified address.
    #[tracing::instrument]
    #[inline]
    pub fn new(server: SocketAddr) -> Result<Self, ToysError> {
        trace!("Setting up a sync DNS client for {}", server);
        let conn =
            UdpClientConnection::new(server).map_err(|err| ToysError::UdpConn(err.to_string()))?;
        Ok(Self {
            client: SyncClient::new(conn),
            server,
        })
    }

    /// Send a TXT query to the dns.toys DNS server, concatenate the response.
    #[tracing::instrument(skip(self))]
    fn query(&self, name_str: &str) -> Result<(TxtRecords, TxtRecords), ToysError> {
        trace!("Sending a query for {}", name_str);
        let name = Name::from_str(name_str)
            .map_err(|err| ToysError::NameFormat(name_str.to_owned(), err.to_string()))?;

        let resp = self
            .client
            .query(&name, DNSClass::IN, RecordType::TXT)
            .map_err(|err| ToysError::Send(name_str.to_owned(), err.to_string()))?;

        Ok((
            super::decode_records(name_str, resp.answers())?,
            super::decode_records(name_str, resp.additionals())?,
        ))
    }

    /// Fetch the current time for the specified city.
    #[tracing::instrument(skip(self))]
    pub fn get_time(&self, city: &City) -> Result<Response<Vec<Time>>, ToysError> {
        trace!("Fetching the time for {} from {}", city, self.server);
        let name_str = format!("{}.time", city);
        let (raw, raw_add) = self.query(&name_str)?;
        parse_time::parse_txt(city, &name_str, raw, &*raw_add)
    }

    /// Fetch the current time for the specified city.
    #[tracing::instrument(skip(self))]
    pub fn get_weather(&self, city: &City) -> Result<Response<Vec<Weather>>, ToysError> {
        trace!(
            "Fetching the weather forecast for {} from {}",
            city,
            self.server
        );
        let name_str = format!("{}.weather", city);
        let (raw, raw_add) = self.query(&name_str)?;
        parse_weather::parse_txt(city, &name_str, raw, &*raw_add)
    }
}
