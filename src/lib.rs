#![deny(missing_docs)]
//! dns-toys-client - bindings for the dns.toys DNS API
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use quick_error::quick_error;
use serde::Serialize;
use trust_dns_client::rr::Record;

pub mod async_tokio;
pub mod parse;
pub mod sync;

quick_error! {
    /// An error that occured while processing the request.
    #[derive(Debug)]
    #[allow(clippy::exhaustive_enums)]
    pub enum ToysError {
        /// The server could not find the requested item (city, etc.).
        ApiNotFound(item: String) {
            display("The server does not know what '{}' is", item)
        }
        /// The server returned an unrecognized error.
        ApiError(err: String) {
            display("The server reported error(s): {}", err)
        }
        /// Could not create an asynchronous DNS client object.
        AsyncClient(err: String) {
            display("Could not create an asynchronous DNS client object")
        }
        /// Could not decode the response string as valid UTF-8.
        Decode(name: String, err: String) {
            display("Could not decode the response to the DNS query for '{}' as valid UTF-8: {}", name, err)
        }
        /// Could not build a valid DNS name.
        NameFormat(name: String, err: String) {
            display("Could not parse {:?} as a valid DNS name: {}", name, err)
        }
        /// Could not parse the DNS response.
        Parse(name: String, err: String) {
            display("Could not parse the response to the DNS query for '{}': {}", name, err)
        }
        /// Could not send the DNS query to the remote server.
        Send(name: String, err: String) {
            display("Could not send the DNS query for '{}': {}", name, err)
        }
        /// Could not initialize a UDP connection.
        UdpConn(err: String) {
            display("Could not create a UDP connection: {}", err)
        }
        /// Something we have not yet done, mmkay?
        NotImplemented(what: String) {
            display("Not implemented yet: {}", what)
        }
    }
}

/// A response fetched from the dns.toys API.
#[derive(Debug)]
pub struct Response<T> {
    /// The raw response in the DNS reply.
    raw: Vec<Vec<String>>,
    /// The parsed response data.
    data: T,
}

impl<T> Response<T> {
    /// The raw response in the DNS reply.
    #[inline]
    pub fn raw(&self) -> &[Vec<String>] {
        &*self.raw
    }

    /// The parsed response data.
    #[inline]
    pub const fn data(&self) -> &T {
        &self.data
    }

    /// Convert into the parsed response data.
    #[inline]
    // https://github.com/rust-lang/rust-clippy/issues/8874
    #[allow(clippy::missing_const_for_fn)]
    pub fn into_data(self) -> T {
        self.data
    }
}

/// A city to fetch information for.
pub type City = String;

/// The current time at some location.
#[derive(Debug, Serialize)]
pub struct Time {
    /// The name of the queried city.
    city: String,
    /// The continent/location timezone name.
    tz_name: String,
    /// The two-letter country code that the city was found in.
    country_code: String,
    /// The current time at that city.
    time: String,
}

impl Time {
    /// The name of the queried city.
    #[inline]
    #[must_use]
    pub fn city(&self) -> &str {
        &self.city
    }

    /// The continent/location timezone name.
    #[inline]
    #[must_use]
    pub fn tz_name(&self) -> &str {
        &self.tz_name
    }

    /// The two-letter country code that the city was found in.
    #[inline]
    #[must_use]
    pub fn country_code(&self) -> &str {
        &self.country_code
    }

    /// The current time at that city.
    #[inline]
    #[must_use]
    pub fn time(&self) -> &str {
        &self.time
    }
}

/// The weather forecast for some location.
#[derive(Debug, Serialize)]
pub struct Weather {
    /// The name of the queried city.
    city: String,
    /// The two-letter country code that the city was found in.
    country_code: String,
    /// The temperature in degrees Celsius.
    temp_celsius: f32,
    /// The temperature in degrees Fahrenheit.
    temp_fahrenheit: f32,
    /// The humidity percentage.
    humidity: f32,
    /// The weather description code.
    weather: String,
    /// The time and day referenced by the forecast.
    time: String,
}

impl Weather {
    /// The name of the queried city.
    #[inline]
    #[must_use]
    pub fn city(&self) -> &str {
        &self.city
    }

    /// The two-letter country code that the city was found in.
    #[inline]
    #[must_use]
    pub fn country_code(&self) -> &str {
        &self.country_code
    }

    /// The temperature in degrees Celsius.
    #[inline]
    #[must_use]
    pub const fn temp_celsius(&self) -> f32 {
        self.temp_celsius
    }

    /// The temperature in degrees Fahrenheit.
    #[inline]
    #[must_use]
    pub const fn temp_fahrenheit(&self) -> f32 {
        self.temp_fahrenheit
    }

    /// The humidity percentage.
    #[inline]
    #[must_use]
    pub const fn humidity(&self) -> f32 {
        self.humidity
    }

    /// The weather code.
    #[inline]
    #[must_use]
    pub fn weather(&self) -> &str {
        &self.weather
    }

    /// The time and day referenced by the weather forecast.
    #[inline]
    #[must_use]
    pub fn time(&self) -> &str {
        &self.time
    }
}

/// The separate strings that comprise a single TXT record.
/// In the dns.toys API, they correspond to the data fields.
type TxtParts = Vec<String>;

/// An array of TXT records split into fields.
type TxtRecords = Vec<TxtParts>;

/// The list of features supported by the library.
pub static FEATURES: [(&str, &str); 3] = [
    ("dns-toys-client", env!("CARGO_PKG_VERSION")),
    ("api-sync", "0.1"),
    ("time", "0.1"),
];

/// Decode a list of TXT records into UTF-8 strings.
fn decode_records(name_str: &str, records: &[Record]) -> Result<TxtRecords, ToysError> {
    records
        .iter()
        .map(|rec| match rec.data() {
            Some(rdata) => match rdata.as_txt() {
                Some(txt_data) => txt_data
                    .txt_data()
                    .iter()
                    .map(|arr| {
                        String::from_utf8(arr.to_vec())
                            .map_err(|err| ToysError::Decode(name_str.to_owned(), err.to_string()))
                    })
                    .collect(),
                None => Err(ToysError::Parse(
                    name_str.to_owned(),
                    format!("unexpected record data type {}", rdata),
                )),
            },
            None => Err(ToysError::Parse(
                name_str.to_owned(),
                "record with no data".to_owned(),
            )),
        })
        .collect::<Result<_, _>>()
}
