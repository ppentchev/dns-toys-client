//! Parser tools for the received DNS records.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use itertools::Itertools;
use regex::{Captures, Regex};
use tracing::error;

use crate::{Response, ToysError, TxtParts, TxtRecords};

pub mod time;
pub mod weather;

mod validate;

/// Parse the "additional" section and build a [`ToysError`] instance.
fn err_from_additional(item: &str, raw_add: &[TxtParts], err_unknown: &str) -> ToysError {
    error!(
        "No data for {}, additional section: {}",
        item,
        raw_add.iter().map(|arr| arr.join("; ")).join("; ")
    );
    let is_not_found = match raw_add.split_first() {
        None => true,
        Some((first, _)) => {
            matches!(**first, [ref single] if single == err_unknown)
        }
    };
    if is_not_found {
        ToysError::ApiNotFound(item.to_owned())
    } else {
        ToysError::ApiError(format!(
            "Could not fetch data for {}: {}",
            item,
            raw_add.iter().map(|arr| arr.join("; ")).join("; ")
        ))
    }
}

/// Parse the fields in the TXT records and validate them.
fn parse_txt<T, F>(
    item: &str,
    name_str: &str,
    raw: TxtRecords,
    raw_add: &[TxtParts],
    err_unknown: &str,
    validators: &[&Regex],
    handle: F,
) -> Result<Response<Vec<T>>, ToysError>
where
    F: Fn(&[Captures]) -> Result<T, ToysError>,
{
    if raw.is_empty() {
        return Err(err_from_additional(item, raw_add, err_unknown));
    }

    let parsed: Vec<Vec<Captures>> = raw
        .iter()
        .map(|parts| {
            if parts.len() == validators.len() {
                parts
                    .iter()
                    .zip(validators.iter())
                    .map(|(part, rex)| -> Result<Captures, ToysError> {
                        rex.captures(part).ok_or_else(|| {
                            ToysError::Parse(
                                name_str.to_owned(),
                                format!("Could not parse {:?}", part),
                            )
                        })
                    })
                    .collect::<Result<Vec<_>, _>>()
            } else {
                Err(ToysError::Parse(
                    name_str.to_owned(),
                    format!(
                        "Unexpected response record: expected {} parts, got {:?}",
                        validators.len(),
                        parts
                    ),
                ))
            }
        })
        .collect::<Result<_, _>>()?;

    let data: Vec<T> = parsed
        .into_iter()
        .map(|caps| handle(&caps))
        .collect::<Result<_, _>>()?;
    Ok(Response { raw, data })
}
