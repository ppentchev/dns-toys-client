//! An asynchronous implementation of the dns.toys client using Tokio.
/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

use std::net::SocketAddr;
use std::str::FromStr;

use tokio::net::UdpSocket;
use tokio::sync::mpsc::Receiver as MpscReceiver;
use tokio::sync::oneshot::Sender as OneshotSender;
use tokio::task::JoinHandle;
use tracing::{error, trace};
use trust_dns_client::client::{AsyncClient, ClientHandle};
use trust_dns_client::proto::error::ProtoError;
use trust_dns_client::rr::{DNSClass, Name, RecordType};
use trust_dns_client::udp::UdpClientStream;

use crate::parse::time as parse_time;
use crate::parse::weather as parse_weather;
use crate::{City, Response, Time, ToysError, Weather};

/// A client for asynchronously sending multiple queries to the dns.toys API.
pub struct ToysClient {
    /// The trust-dns-client object for sending the queries.
    client: AsyncClient,
    /// The trust-dns-client background task.
    task: Option<JoinHandle<Result<(), ProtoError>>>,
    /// The server's address.
    server: SocketAddr,
}

/// A single request for the client to send out.
#[derive(Debug)]
#[allow(clippy::exhaustive_enums)]
pub enum Request {
    /// Query the time at the specified location.
    Time(City, OneshotSender<Result<Response<Vec<Time>>, ToysError>>),
    /// Query the weather forecast for the specified location.
    Weather(
        City,
        OneshotSender<Result<Response<Vec<Weather>>, ToysError>>,
    ),
    /// Time to go away.
    Done,
}

impl ToysClient {
    /// Prepare to send queries to the dns.toys API server at the specified address.
    ///
    /// # Errors
    /// Could not prepare a UDP connection to the specified address.
    #[tracing::instrument]
    #[inline]
    pub async fn new(server: SocketAddr) -> Result<Self, ToysError> {
        trace!("Setting up a sync DNS client for {}", server);
        let conn = UdpClientStream::<UdpSocket>::new(server);
        let (client, bg) = AsyncClient::connect(conn)
            .await
            .map_err(|err| ToysError::AsyncClient(err.to_string()))?;
        let task = tokio::spawn(bg);
        Ok(Self {
            client,
            task: Some(task),
            server,
        })
    }

    /// Run a loop to process messages.
    #[tracing::instrument(skip(self, rx))]
    pub async fn process(&mut self, mut rx: MpscReceiver<Request>) {
        trace!("Starting the client loop");
        while let Some(msg) = rx.recv().await {
            trace!("Got a message: {:?}", msg);
            match msg {
                Request::Done => break,
                Request::Time(city, tx) => {
                    trace!("Got a time request for '{}'", city);
                    let resp = self.get_time(&city).await;
                    if tx.send(resp).is_err() {
                        error!(
                            "Could not send the response for the 'time' query for '{}'",
                            city
                        );
                    }
                }
                Request::Weather(city, tx) => {
                    trace!("Got a weather request for '{}'", city);
                    let resp = self.get_weather(&city).await;
                    if tx.send(resp).is_err() {
                        error!(
                            "Could not send the response for the 'weather' query for '{}'",
                            city
                        );
                    }
                }
            }
        }
        trace!("Out of the client loop");
    }

    /// Send a query to the dns.toys server, extract the "answer" and "additional" sections.
    #[tracing::instrument(skip(self))]
    async fn query(
        &mut self,
        name_str: &str,
    ) -> Result<(Vec<Vec<String>>, Vec<Vec<String>>), ToysError> {
        trace!("About to send a {} query", name_str);
        let name = Name::from_str(name_str)
            .map_err(|err| ToysError::NameFormat(name_str.to_owned(), err.to_string()))?;

        let resp = self
            .client
            .query(name, DNSClass::IN, RecordType::TXT)
            .await
            .map_err(|err| ToysError::Send(name_str.to_owned(), err.to_string()))?;

        Ok((
            super::decode_records(name_str, resp.answers())?,
            super::decode_records(name_str, resp.additionals())?,
        ))
    }

    /// Fetch the current time for the specified city.
    #[tracing::instrument(skip(self))]
    pub async fn get_time(&mut self, city: &City) -> Result<Response<Vec<Time>>, ToysError> {
        trace!("Fetching the time for {} from {}", city, self.server);
        let name_str = format!("{}.time", city);
        let (raw, raw_add) = self.query(&name_str).await?;
        parse_time::parse_txt(city, &name_str, raw, &*raw_add)
    }

    /// Fetch the weather forecast for the specified city.
    #[tracing::instrument(skip(self))]
    pub async fn get_weather(&mut self, city: &City) -> Result<Response<Vec<Weather>>, ToysError> {
        trace!(
            "Fetching the weather forecast for {} from {}",
            city,
            self.server
        );
        let name_str = format!("{}.weather", city);
        let (raw, raw_add) = self.query(&name_str).await?;
        parse_weather::parse_txt(city, &name_str, raw, &*raw_add)
    }
}

impl Drop for ToysClient {
    /// Stop the resolver thread.
    #[tracing::instrument(skip(self))]
    fn drop(&mut self) {
        if let Some(task) = self.task.take() {
            task.abort();
        }
    }
}
